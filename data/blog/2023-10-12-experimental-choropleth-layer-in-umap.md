---
title: Experimental choropleth layer in uMap
date: 2023-10-12T10:07:07
source: https://www.openstreetmap.org/user/ybon/diary/402589
---

<p>We’ve just released the version 1.9.2 of uMap, that includes a new experimental type of layer: choropleth!</p>

<p><img alt="choropleth map" src="https://i.imgur.com/7Ek2EgX.png" /></p>

<p>To test it, just select this new type in the dropdown</p>

<p><img alt="dropdown" src="https://i.imgur.com/UdskTwC.png" /></p>

<p>Then you’ll find new advanced properties to configure it:</p>

<p><img alt="advanced properties" src="https://i.imgur.com/H6CHvjI.png" /></p>

<p>Among those properties, the only mandatory is the “property value”, that will tell uMap which layer property to use for computing the choropleth classes.</p>

<p>Optionally, you can define the color palette to use (they come from the <a href="https://colorbrewer2.org/#type=sequential&amp;scheme=BuGn&amp;n=3" rel="nofollow noopener noreferrer">color brewer</a> ones) the number of classes you want and the algorithm (from <a href="https://simple-statistics.github.io/" rel="nofollow noopener noreferrer">simple-statistics</a> to use for computing the class breaks (which you can also define by hand in the raw input).</p>

<p>It’s quite experimental, so please test it and give feedback!</p>

<p>Other <a href="https://umap-project.readthedocs.io/en/master/changelog/#190-2023-10-12" rel="nofollow noopener noreferrer">changes</a> in this release include the ability to hide a layer from the caption and a few enhancements for heatmap layers (<a href="https://github.com/Leaflet/Leaflet.heat/pull/78" rel="nofollow noopener noreferrer">context</a>).</p>
