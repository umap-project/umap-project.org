---
title: uMap 2 and beyond 🚀
date: 2024-02-23T10:07:07
source: https://www.openstreetmap.org/user/David%20Larlet/diary/403560
---

<p>A <a href="https://pypi.org/project/umap-project/2.0.0/" rel="nofollow noopener noreferrer">major version</a> of uMap has been released last week.</p>

<p>This release is inauguring a new era in versioning uMap: in the future, we’ll take care of better documenting breaking changes, so expect more major releases from now on. More details on <a href="https://docs.umap-project.org/en/master/release/#when-to-make-a-release" rel="nofollow noopener noreferrer">how we version</a>.</p>

<p>A <a href="https://docs.umap-project.org/en/master/changelog/#200-2024-02-16" rel="nofollow noopener noreferrer">comprehensive changelog</a> for that version is available in our technical documentation. Most of the changes for a major version are indeed technical, we are taking care of people deploying and maintaining instances with that approach. User-facing features are deployed continuously with our minor versions. We think that scheme is more valuable for the community.</p>

<p>It doesn’t mean you will not see improvements with that release though. Most notably, you’ll face:</p>

<h4 id="a-revamped-dashboard-with-multiple-options-for-your-maps-including-a-bulk-download--backup">A revamped dashboard with multiple options for your maps (including a bulk download / backup)</h4>

<p>A hard work by <a href="https://github.com/davidbgk" title="GitHub User: davidbgk" rel="nofollow noopener noreferrer">@davidbgk</a> and <a href="https://github.com/Aurelie-Jallut" title="GitHub User: Aurelie-Jallut" rel="nofollow noopener noreferrer">@Aurelie-Jallut</a>!</p>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/465d13f8-64ac-41f9-850a-4b3bae1050a5.png" alt="revamped dashboard"></p>

<h4 id="experimental-drag-and-drop-of-file-on-the-map-container">Experimental drag and drop of file on the map container</h4>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/99cb5dc7-eb32-4fd2-bcc2-ed0d8e357436.gif" alt="Experimental drag and drop of file on the map container"></p>

<h4 id="add-minimal-csv-export">Add minimal CSV export</h4>

<p>This sounds like this export should have existed before, but until now we did not decide what to do with complex geometries, and now we did: for now it just add the center. Let’s use it and see if we need something more complex.</p>

<h4 id="highlight-selected-feature-finally">Highlight selected feature, finally!</h4>

<p>A contribution of <a href="https://github.com/jschleic" title="GitHub User: jschleic" rel="nofollow noopener noreferrer">@jschleic</a>, with the help of <a href="https://github.com/Aurelie-Jallut" title="GitHub User: Aurelie-Jallut" rel="nofollow noopener noreferrer">@Aurelie-Jallut</a>!</p>

<h4 id="full-map-download-endpoint">Full map download endpoint</h4>

<p>You can now backup your map programmatically! The URL should look like <a href="https://yourumapdoma.in/map/{yourid}/download/" rel="nofollow noopener noreferrer">https://yourumapdoma.in/map/{yourid}/download/</a></p>

<h4 id="refactor-icon-image-selector">Refactor icon image selector</h4>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/136b866f-5a3b-4ef0-9d56-04177f4d5ddc.gif" alt="Changing an icon image in uMap"></p>

<h4 id="very-minimal-optimistic-conflicts-resolution-mechanism">Very minimal optimistic conflicts resolution mechanism</h4>

<p>Thanks to <a href="https://github.com/almet" title="GitHub User: almet" rel="nofollow noopener noreferrer">@almet</a> for this, and to <a href="https://github.com/Biondilbiondo" title="GitHub User: Biondilbiondo" rel="nofollow noopener noreferrer">@Biondilbiondo</a> for the initial idea and implementation</p>

<p>See also the work in progress (<strong>🚧 meaning not yet in production</strong>) by <a href="https://github.com/almet" title="GitHub User: almet" rel="nofollow noopener noreferrer">@almet</a> to make <a href="http://blog.notmyidea.org/adding-collaboration-on-umap-third-update.html" rel="nofollow noopener noreferrer">live collaborative editing</a> a thing in uMap.</p>

<h4 id="new-tilelayer-switcher-thanks-to-leafleticonlayers">New tilelayer switcher (thanks to Leaflet.IconLayers)</h4>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/369c2908-b5b1-4281-b66c-1396c122f10b.gif" alt="A demo of background switch for a map"></p>

<h4 id="add-counter-in-datalayer-headline">Add counter in datalayer headline</h4>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/e315e255-d66e-4c81-9fd6-d540f259d2e7.png" alt="A screenshot of datalayers with a counter on the right side"></p>

<h4 id="allow-to-type-a-latlng-in-the-search-box">Allow to type a latlng in the search box</h4>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/224ed999-5c95-4d50-9c62-1d2feab78804.png" alt="A screenshot of latitude and longitude search in uMap"></p>

<h4 id="add-a-popup-template-to-showcase-openstreetmap-data">Add a popup template to showcase OpenStreetMap data</h4>

<p>The idea is to easily show the most common tags in OSM (name, email, address…). It’s very basic for now, so let’s <a href="https://forum.openstreetmap.fr/t/mieux-integrer-osm-dans-umap/19127" rel="nofollow noopener noreferrer">make it better together!</a></p>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/060129ab-de3d-405e-a9a0-77021e4523ea.png" alt="Showcasing a popup for a bakery from OSM data"></p>

<h4 id="refactor-share--download-ui-for-better-usability">Refactor Share &amp; Download UI for better usability</h4>

<p>Thanks to <a href="https://github.com/jschleic" title="GitHub User: jschleic" rel="nofollow noopener noreferrer">@jschleic</a> for this contribution!</p>

<p><img src="https://storage.gra.cloud.ovh.net/v1/AUTH_0f20d409cb2a4c9786c769e2edec0e06/padnumerique/uploads/f1902c67-0e8a-44c6-bf09-dec3f6e1a3c4.png" alt="A screenshot of the share panel of uMap"></p>

<h4 id="and-even-more">And even more</h4>

<ul>
  <li>an attention to accessibility and contrasts (more to come!)</li>
  <li>the ability to use oEmbed</li>
</ul>

<h2 id="thank-you-note">Thank you note</h2>

<p>Since May 2023, uMap has received support from 2 French government agencies: <a href="https://agence-cohesion-territoires.gouv.fr/" rel="nofollow noopener noreferrer">Agence Nationale de la Cohésion des Territoires</a> and <a href="https://communs.beta.gouv.fr/" rel="nofollow noopener noreferrer">Accélérateur d’initiatives citoyennes</a>. Also, a NGI Grant from the <a href="https://nlnet.nl/" rel="nofollow noopener noreferrer">NLNet foundation</a> is in progress to work on the collaborative editing feature. A <a href="https://umap.incubateur.anct.gouv.fr/fr/" rel="nofollow noopener noreferrer">dedicated instance</a> has been up for a couple month already useful to close to a hundred public agents. This has allowed to finance a team of 7 to work part-time on the project. We’ve recently received confirmation that the funding would be continued in 2024 so keep expecting a lot of activity on the project!</p>

<p>The team is still dedicated to make uMap more stable, accessible and we welcome newcomers so do not hesitate to:</p>

<ul>
  <li><a href="https://app.element.io/#/room/#umap:matrix.org" rel="nofollow noopener noreferrer">join us on Matrix</a>,</li>
  <li>contribute <a href="https://github.com/umap-project/umap/" rel="nofollow noopener noreferrer">to the code</a>,</li>
  <li>and/or give us your feedback through  <a href="https://lists.openstreetmap.org/listinfo/umap" rel="nofollow noopener noreferrer">the mailing-list</a> (en) or <a href="https://forum.openstreetmap.fr/c/utiliser/umap/29" rel="nofollow noopener noreferrer">the forum</a> (fr).</li>
</ul>

<p>Also don’t miss out on the revamped <a href="https://docs.umap-project.org/en/master/" rel="nofollow noopener noreferrer">developer docs website</a> and our the brand <a href="https://umap-project.org/" rel="nofollow noopener noreferrer">new official uMap Project website</a>.</p>

<p>If you appreciate our work and want to support the team behind it please consider donating on:</p>

<ul>
  <li><a href="https://liberapay.com/uMap" rel="nofollow noopener noreferrer">Libera Pay</a></li>
  <li><a href="https://opencollective.com/uMap" rel="nofollow noopener noreferrer">Open Collective</a></li>
</ul>

<p><em>The uMap team</em></p>
