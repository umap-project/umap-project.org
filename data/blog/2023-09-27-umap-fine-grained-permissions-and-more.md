---
title: "uMap: fine-grained permissions and more"
date: 2023-09-27T12:03:04
source: https://www.openstreetmap.org/user/David%20Larlet/diary/402475
---

<p><em>In the <a href="https://www.openstreetmap.org/user/ybon/diary/402248" rel="nofollow noopener noreferrer">previous episode</a>…</em></p>

<p>We finally managed to tackle a very popular feature request: <a href="https://github.com/umap-project/umap/pull/1307" rel="nofollow noopener noreferrer">datalayers’ fine-grained permissions</a> 🎉. This is a huge step forward, allowing for a given map owner to <em>only</em> open a particular datalayer to edition. It will help people with contributive maps who need to setup a stable/fixed base layer. It also paved the way for even more control over the objects that are allowed for addition and/or edition. Please share with us your desired workflows.</p>

<p><img alt="Two datalayers with different permissions" src="https://github.com/umap-project/umap/assets/3556/7b402cf4-888c-41b3-8640-f92bc6e16294" /></p>

<p>On the UX side of the project, we made a couple of adjustments  and fixes to make the editor more intuitive and consistent. Do you see these new crispy icons on the screenshot above? Hopefully it will bring more users, hence more contributors! A couple of <a href="https://github.com/umap-project/umap/graphs/contributors" rel="nofollow noopener noreferrer">new faces jumped in</a> recently and we’re so happy about that 🤗.</p>

<p>You can also look up for icons by name in the ‘Shape properties’ panel, one of our next steps will be to ease icons’ management and additions, another long-awaited feature:</p>

<p><img alt="An example of icons search" src="https://github.com/umap-project/umap/assets/3556/29e87b6a-333a-4ed2-ab24-40e934922ea5" /></p>

<p>We receive a lot of feedback (and bug reports) from the <a href="https://forum.openstreetmap.fr/c/utiliser/umap/29" rel="nofollow noopener noreferrer">OSM France forum</a> too which definitely helps us to improve the product in terms of user experience and to prioritize the roadmap. If you are working for the French public sector, do not hesitate <a href="https://forum.openstreetmap.fr/t/vous-utilisez-umap-pour-une-mission-de-service-public/17713" rel="nofollow noopener noreferrer">to reach out and share your experience</a>.</p>

<p>Stay tuned for the next additions, a brand new API is coming! We’ll be at the <a href="https://numerique-en-communs.fr" rel="nofollow noopener noreferrer">NEC - Numérique En Communs</a> event (Bordeaux, France), on October 19th and 20th. See you there for more news and announcements!</p>
